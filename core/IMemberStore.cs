﻿namespace core
{
    public interface IMemberStore
    {
        void Add(ClubMember guest);
    }
}
