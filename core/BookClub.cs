﻿
namespace core
{
    public class BookClub
    {
        private IMemberStore memberStore;
        private IBookStore bookStore;
        private IRequestStore requestStore;

        public BookClub(IMemberStore memberStore, IBookStore bookStore, IRequestStore requestStore)
        {
            this.bookStore = bookStore;
            this.memberStore = memberStore;
            this.requestStore = requestStore;
        }

        public void AddMember(ClubMember member)
        {
            memberStore.Add(member);
        }

        public void ShareBook(Book book)
        {
            bookStore.Publish(book);
        }

        public void SearchBook(string title)
        {
            bookStore.Search(title);
        }

        public void BorrowBook(Book book, ClubMember borrower)
        {
            requestStore.Register(book, borrower);
        }
    }
}
