﻿using System.Collections.Generic;
using System.Linq;

namespace core
{
    public class BookStore : IBookStore
    {
        private List<Book> books;
        public BookStore()
        {
            books = new List<Book>();
        }

        public void Publish(Book book)
        {
            books.Add(book);
        }

        public IEnumerable<Book> Search(string title = "")
        {
            if (string.IsNullOrWhiteSpace(title))
                return books;

            return books
                .Where(x => x.Title.ToLower().Contains(title.ToLower()))
                .Distinct(new BookComparer());
        }
    }
}
