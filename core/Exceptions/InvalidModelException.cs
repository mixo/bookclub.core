﻿using System;

namespace core
{
    public class InvalidModelException<TModel> : Exception
    {
        public TModel Model { get; private set; }
        public InvalidModelException(TModel model)
        {
            this.Model = model;
        }
    }
}