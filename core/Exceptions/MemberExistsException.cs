﻿using System;

namespace core
{
    public class MemberExistsException : Exception
    {
        public MemberExistsException(string message) : base(message) { }
    }
}
