﻿
using System.Collections.Generic;
namespace core
{
    public class Book
    {
        public string Title { get; set; }
    }

    public class BookComparer : IEqualityComparer<Book>
    {
        public bool Equals(Book x, Book y)
        {
            return x.Title.ToLower() == y.Title.ToLower();
        }

        public int GetHashCode(Book obj)
        {
            return obj.Title.ToLower().GetHashCode();
        }
    }
}
