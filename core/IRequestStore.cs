﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core
{
    public interface IRequestStore
    {
        void Register(Book book, ClubMember borrower);
    }
}
