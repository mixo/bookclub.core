﻿
using System.Collections;
using System.Collections.Generic;
namespace core
{
    public interface IBookStore
    {
        void Publish(Book book);

        IEnumerable<Book> Search(string existingTitle);
    }
}
