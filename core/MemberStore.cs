﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace core
{
    public class MemberStore : IMemberStore
    {
        public ICollection<ClubMember> Members { get; private set; }
        public MemberStore()
        {
            Members = new List<ClubMember>();
        }

        public void Add(ClubMember member)
        {
            if (member == null) { throw new ArgumentNullException(); }
            if (String.IsNullOrEmpty(member.Email)) { throw new InvalidModelException<ClubMember>(member); }
            if (Members.Any(x => x.Email == member.Email))
                throw new MemberExistsException("Already Member");

            Members.Add(member);
        }
    }
}
