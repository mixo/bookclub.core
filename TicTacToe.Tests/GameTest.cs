﻿
using Xunit;

namespace TicTacToe.Tests
{

    public class GameTest
    {
        [Fact]
        public void GameShouldStartWithEmptyBoard()
        {
            var game = new Game();
            string[,] board = game.GetBoard();
            Assert.Equal(new string[3, 3] { 
                { "", "", "" }, 
                { "", "", "" }, 
                { "", "", "" }
            }, board);
        }

        [Fact]
        public void FirstPlayerShouldBeX()
        {
            var game = new Game();
            game.PlayAt(BoardCoordinateX.Left, BoardCoordinateY.Top);
            Assert.Equal(new string[3, 3] { 
            { "X", "", "" }, 
            { "", "", "" }, 
            { "", "", "" } },
            game.GetBoard());
        }

        //second move should be O

        //game should be able to start with X or O ( any other symbol should be discarded )

        //game should not allow playing same point twice

        //game win ( row win, col win, diagonal win )

        //game draw state

        //game should allow reset
    }
}
