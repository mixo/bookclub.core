﻿
using FluentAssertions;
using System;
using Xunit;

namespace core.test
{
    public class MemberStoreTests
    {
        private MemberStore store;
        public MemberStoreTests()
        {
            store = new MemberStore();
        }

        [Fact]
        public void WhenStoreIsCreated_ShouldBeEmpty()
        {
            var members = store.Members;
            members.Should().BeEmpty();
        }

        [Fact]
        public void WhenAddingNewMember_ShouldStoreItInSystem()
        {
            var guest = new ClubMember() { Email = "beqa@helix.ge" };
            store.Add(guest);
            store.Members.Should().HaveElementAt(0, guest);
        }

        [Fact]
        public void WhenAlreadyMember_ThenShouldNotBecomeMemberAgain()
        {
            var guest = new ClubMember() { Email = "beqa@helix.ge" };
            var member = new ClubMember() { Email = "beqa@helix.ge" };

            store.Add(guest);
            Action addMember = () => store.Add(member);

            addMember.ShouldThrow<MemberExistsException>().WithMessage("Already Member");
        }

        [Fact]
        public void WhenMemberIsNull_ShouldThrowArgumentNullException()
        {
            Action invalidMember = () => store.Add(null);
            invalidMember.ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void WhenMemberIsNotValid_ShouldThrowInvalidModelException()
        {
            Action invalidMember = () => store.Add(new ClubMember());
            invalidMember.ShouldThrow<InvalidModelException<ClubMember>>()
                .And.Should().Match<InvalidModelException<ClubMember>>(x => string.IsNullOrWhiteSpace(x.Model.Email));
        }
    }
}
