﻿using FluentAssertions;
using Xunit;
namespace core.test
{
    public class BookStoreTests
    {
        private BookStore _store;
        [Fact]
        public void WhenStoreIsCreated_ShouldBeEmpty()
        {
            var _store = new BookStore();
            _store.Search().Should().BeEmpty();
        }

        [Theory]
        [InlineData("Code", 2)]
        [InlineData("legacy", 2)]
        [InlineData("tdd", 1)]
        [InlineData("domain", 0)]
        [InlineData("", 6)]
        public void WhenSearching_ShouldSearchByTitle(string searchKeyword, int resultCount)
        {
            _store = new BookStore();
            _store.Publish(new Book { Title = "Code Complete" });
            _store.Publish(new Book { Title = "Design Patterns" });
            _store.Publish(new Book { Title = "TDD By Examples" });
            _store.Publish(new Book { Title = "tdd by examples" });
            _store.Publish(new Book { Title = "Refactoring Legacy Systems" });
            _store.Publish(new Book { Title = "Working With Legacy Code" });
            _store.Search(searchKeyword).Should().HaveCount(resultCount);
        }
    }
}
