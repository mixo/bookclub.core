﻿
using NSubstitute;
using Xunit;

namespace core.test
{
    public class GivenBookClub
    {
        protected IMemberStore memberStore;
        protected IBookStore bookStore;
        protected IRequestStore requestStore;
        protected BookClub club;
        public GivenBookClub()
        {
            memberStore = Substitute.For<IMemberStore>();
            bookStore = Substitute.For<IBookStore>();
            requestStore = Substitute.For<IRequestStore>();
            club = new BookClub(memberStore, bookStore, requestStore);
        }
        public class WhenSharingBook : GivenBookClub
        {
            [Fact]
            public void ThenBookIsShared()
            {
                var book = new Book();
                club.ShareBook(book);
                bookStore.Received().Publish(book);
            }
        }

        public class WhenAddingMember : GivenBookClub
        {
            [Fact]
            public void WhenGuest_ThenShouldBecomeMember()
            {
                var guest = new ClubMember();
                club.AddMember(guest);
                memberStore.Received().Add(guest);
            }
        }

        public class WhenSearchingBook : GivenBookClub
        {
            [Fact]
            public void ThenShouldSearchTheBook()
            {
                var existingTitle = "Code Complete";
                club.SearchBook(existingTitle);
                bookStore.Received().Search(existingTitle);
            }
        }

        public class WhenBorrowingBook : GivenBookClub
        {
            [Fact]
            public void ThenSystemShouldRegisterBookRequest()
            {
                var book = new Book();
                var borrower = new ClubMember();
                club.BorrowBook(book, borrower);
                requestStore.Received().Register(book, borrower);
            }
        }
    }
}
