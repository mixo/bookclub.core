﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public enum BoardCoordinateX
    {
        Left = 0,
        Middle = 1,
        Right = 2
    }
    public enum BoardCoordinateY
    {
        Top = 0,
        Middle = 1,
        Bottom = 2
    }
}
