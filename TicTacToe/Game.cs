﻿
namespace TicTacToe
{
    public class Game
    {
        string[,] board;

        public Game()
        {
            board = new string[3, 3] {
                {"", "", ""},
                {"", "", ""},
                {"", "", ""}
            };
        }

        public string[,] GetBoard()
        {
            return board;
        }

        public void PlayAt(BoardCoordinateX boardCoordinateX, BoardCoordinateY boardCoordinateY)
        {
            board[0, 0] = "X";
        }
    }
}
